class PublicController {
    constructor(app) {
        this.app = app;
    }

    async index(req, res) {
        return res.end('welcome');
    }

    async sayHelloworld(req, res) {
        return res.end('hello world');
    }

    async listUsers(req, res) {
        const users = await UsersModel.find({});

        return res.json(users);
    }

    async publish(req, res) {
        /*if (!(req.body.name && req.body.sex)) {
            res.status(400).send({ error: 'the body must contain the name prop and sex prop' });
        }*/

        await SLExpressTest.enqueue({ name: 'aric', age: 27 });

        res.status(200).send({});
    }
}

module.exports = PublicController;