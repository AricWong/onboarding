module.exports = function(users) {
    return users.map((user) => {
        user.company = 'shopline';
        return user;
    });
}