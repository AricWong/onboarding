const http2 = require('http2');

let _sharedGoogleApi = null;
const HTTP_STATUS = ':status';

class GoogleApi {
    static get sharedGoogleApi() {
        if (!_sharedGoogleApi) {
            _sharedGoogleApi = new GoogleApi();
        }

        return _sharedGoogleApi;
    }

    init(config) {
        this.endpoint = config.endpoint;
        this.listUsersPath = config.listUsersPath;

        return this;
    }

    async connect() {
        const self = this;
        const p = new Promise((resolve, reject) => {
            const c = http2.connect(self.endpoint);
            
            c.on('connect', () => {
                resolve(c);
            })
            c.on('error', (e) => {
                reject(e);
            })
        });

        this.c = await p;
    }

    async disconnect() {
        this.c.close();
    }

    async listUsers() {
        const self = this;
        
        return new Promise((resolve, reject) => {
            let data = '';
            const req = self.c.request({
                ':path': self.listUsersPath,
            });

            req.on('response', (headers) => {
                const statusCode = headers[HTTP_STATUS];
                if (200 != statusCode) {
                    reject(`GET ${self.endpoint}${self.listUsersPath} ${statusCode}`);
                }
            });

            req.setEncoding('utf8');
            req.on('data', (chunk) => {
                data += chunk;
            })
            req.on('end', () => {
                resolve(JSON.parse(data));
            });

            req.on('error', (e) => {
                reject(e);
            });
        });
    }
}

module.exports = GoogleApi;