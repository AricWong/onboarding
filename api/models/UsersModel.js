/*
const db = [];

class UsersModel {
    static async setUsers(users) {
        for(let user of users) {
            db.push(user);
        }
    }

    static async getUsers() {
        return db;
    }
}
*/

class UsersModel extends MongooseModel {
    static schema() {
        return {
            id: Number,
            userId: Number,
            title: String,
            completed: Boolean,
            company: String,  
        }
    }

    static async insertUsers(users) {
        const self = this;

        return new Promise((resolve, reject) => {
            self.insertMany(users, (err, docs) => {
                if (err) {
                    return reject(err);
                }

                return resolve(docs);
            });
        });
    }

    static async listUsers(filter) {
        const self = this;

        return new Promise((resolve, reject) => {
            const query = self.find(filter);

            query.exec((err, docs) => {
                if (err) {
                    return reject(err);
                }

                return resolve(docs);
            });
        });
    }
}

module.exports = UsersModel;