class SLExpressTest {
    static async enqueue(msg) {
        const payload = {
            name: msg.name,
            age: msg.age,
        };

        await QueueTask.queue({
            taskType: 'SLEXPRESS_TEST',
            payload: payload
        });
    }

    static async dequeue(queueTask) {
        const payload = queueTask.payload;

        console.log(payload);
    }
}

module.exports = SLExpressTest;