module.exports = {
    preMiddlewares: [
        '* requestLog requestParseURLEncoded requestParseBody',
    ],
    routes: [
        'GET / PublicController.index',
        'GET /helloworld PublicController.sayHelloworld',
        'GET /users PublicController.listUsers',
        'POST /publish PublicController.publish'
    ],
    postMiddlewares: [],
}
