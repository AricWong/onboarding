module.exports = {
    appenders: {
        slexpress_test: {
            type: 'file',
            filename: 'logs/slexpress_test.log'
        },
    },
    categories: {
        default: {
            appenders: [
                'slexpress_test',
            ],
            level: 'info'
        },
    },
}