module.exports = {
    endpoint: process.env.GOOGLE_API_ENDPOINT,
    listUsersPath: process.env.GOOGLE_API_LIST_USERS_PATH,
}