const app = require('./app.js');

app.start().catch((e) => {
    console.error(e);
    process.exit(1);
});