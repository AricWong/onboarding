const TestCombo = require('../../TestCombo');
const googleApiHelper = require('../../../api/helpers/googleApiHelper');

class TestSuite extends TestCombo {
    get title() {
        return 'googleApiHelper';
    }

    get args() {
        return ['users'];
    }

    get argTypes() {
        return {
            users: ['correct']
        }
    }

    filter(combination) {
        return true;
    }

    extraCombinations() {
        return [];
    }

    beforeAll(test, combination) {}

    beforeEach(test, combination) {
        return this.runTest(test, combination)
    }

    afterAll(test, combination) { }

    afterEach(test, combination) {}

    getArgValues(test, combination, arg, argType) {
        const argValues = {
            users: {
                correct: [
                    {
                        userId: '123',
                    },
                ]
            }
        }

        return argValues[arg][argType];
    }

    testMethod(test, combination, argValues) {
        const [users] = argValues;

        return googleApiHelper(users);
    }

    shouldSuccess(combination) {
        const [users] = combination;

        return users.match(/correct/);
    }

    successAssert(test, combination) {
        it('user object should has company property', () => {
            expect(test.res[0].company).toEqual('shopline');
        })
    }

    failureAssert(test, combination) { }
}

const testSuite = new TestSuite()
testSuite.run()