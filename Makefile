.PHONY: start stop test
start:
	docker build -t slexpress:test . && docker-compose up -d
stop:
	docker-compose down
test:
	npm run test
