const { App: SuperApp } = require('@shopline/sl-express');

class App extends SuperApp {
    async connectGoogleApi() {
        await GoogleApi.sharedGoogleApi.init(this.config.googleApi).connect();
    }

    async initUsersModel() {
        const users = await GoogleApi.sharedGoogleApi.listUsers();
        await this.models.UsersModel.insertUsers(helpers.googleApiHelper(users));
    }

    async disconnectGoogleApi() {
        await GoogleApi.sharedGoogleApi.disconnect();
    }

    async connectDependencies() {
        try {
            await super.connectDependencies();
            await this.connectGoogleApi();
            await this.initUsersModel();
        } catch(e) {
            throw e;
        }
    }

    async disconnectDependencies() {
        try {
            await this.disconnectGoogleApi();
            //await this.disconnectRedis();
            await super.disconnectDependencies();
        } catch(e) {
            throw e;
        }
    }

    async startService() {
        if (this.config.app.role === 'CONSUMER') {
            return await this.startConsumer();
        }

        await this.startExpress();
    }
}

const app = new App();

module.exports = app;