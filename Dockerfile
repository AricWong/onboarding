FROM node:8.9.1-slim

ENV APP_DIR=/app

COPY package.json $APP_DIR/package.json

RUN cd $APP_DIR \
    && npm install

COPY . $APP_DIR

WORKDIR $APP_DIR

EXPOSE 3000

CMD node server.js